import webpack from 'webpack'
import merge from 'webpack-merge'
import base from './webpack.base.config'
import VueSSRClientPlugin from 'vue-server-renderer/client-plugin'
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const config = merge(base, {
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 50000,
      maxSize: 70000,
      minChunks: 1,
      maxAsyncRequests: 100,
      maxInitialRequests: 100,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          name (module) {
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
            return `npm.${packageName.replace('@', '')}`;
          },
          priority: -10,
          reuseExistingChunk: true,
          minSize: 50000,
          maxSize: 70000
        },
        common: {
          test: /[\\/]src[\\/]modules[\\/]/,
          chunks: 'all',
          minSize: 50000,
          maxSize: 70000,
          reuseExistingChunk: true
        },
        core: {
          test: /[\\/]core[\\/]modules[\\/]/,
          chunks: 'all',
          minSize: 50000,
          maxSize: 70000,
          reuseExistingChunk: true
        }
      }
    },
    runtimeChunk: {
      name: 'manifest'
    }
  },
  mode: 'development',
  resolve: {
    alias: {
      'create-api': './create-api-client.js'
    }
  },
  plugins: [
    // new BundleAnalyzerPlugin(),
    // strip dev-only code in Vue source
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': '"client"'
    }),
    new VueSSRClientPlugin()
  ]
})

export default config;
