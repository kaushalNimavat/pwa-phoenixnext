import { currentStoreView } from '@vue-storefront/core/lib/multistore'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timeZone from 'dayjs-ext/plugin/timeZone'
import dayjsLocalizedFormat from 'dayjs/plugin/localizedFormat'
import { once } from '../helpers';

once('__VUE_EXTEND_DAYJS_LOCALIZED_FORMAT__', () => {
  dayjs.extend(dayjsLocalizedFormat)
  dayjs.extend(utc)
  dayjs.extend(timeZone)
})

/**
 * Converts date to format provided as an argument or defined in config file (if argument not provided)
 * @param {String} date
 * @param {String} format
 */
export function date (date, format, storeView) {
  const _storeView = storeView || currentStoreView()
  const displayFormat = format || _storeView.i18n.dateFormat
  let storeLocale = _storeView.i18n.defaultLocale.toLocaleLowerCase()
  const separatorIndex = storeLocale.indexOf('-')
  const languageCode = (separatorIndex > -1) ? storeLocale.substr(0, separatorIndex) : storeLocale

  const isStoreLocale = dayjs().locale(storeLocale).locale()
  const isLanguageLocale = dayjs().locale(languageCode).locale()
  const locale = isStoreLocale || isLanguageLocale

  if (locale) return dayjs.utc(date).locale(languageCode).format(displayFormat, { timeZone: 'Asia/Bangkok' })
  return dayjs.utc(date).format(displayFormat, { timeZone: 'Asia/Bangkok' })
}
