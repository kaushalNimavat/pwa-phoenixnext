import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.ADDRESS_ADD] (state, customer) {
    state.customer = customer || []
  },

  [types.ADDRESS_UPDATE] (state, customer) {
    state.customer = customer || []
  },

  [types.ADDRESS_DELETE] (state, customer) {
    state.customer = customer || []
  }
}
