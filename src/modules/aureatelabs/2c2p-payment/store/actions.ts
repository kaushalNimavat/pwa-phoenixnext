import { ActionTree } from 'vuex';
import PaymentState from '../types/PaymentState'
import rootStore from '@vue-storefront/core/store'
import Payment from '../types/Payment';
import { Base64 } from 'js-base64';

var crypto = require('crypto');

function getGrandTotals () { // To get grand total value from cartTotals getters
  const totalsGetters = rootStore.getters['cart/getTotals']
  const grandTotal = totalsGetters.find(field => field.code === 'grand_total')
  return grandTotal.value
}

function formatPrice (price) { // To format price according to 2c2p payment method
  var my_string = '' + Number(price).toFixed(2).replace('.', '');
  while (my_string.length < 12) {
    my_string = '0' + my_string;
  }
  return my_string;
}

const actions: ActionTree<PaymentState, any> = {
  processPayment ({ dispatch }, PaymentData: Payment) {
    var merchantID = rootStore.state.config.aureatelabs.payment.merchantID;
    var secretKey = rootStore.state.config.aureatelabs.payment.secretKey;
    var desc = rootStore.state.config.aureatelabs.payment.desc;
    var uniqueTransactionCode = Date.now();
    var currencyCode = rootStore.state.config.aureatelabs.payment.currencyCode;
    let price = getGrandTotals()
    var amt = formatPrice(price);
    var storeCard = 'Y';
    // var amt = formatPrice(rootStore.getters['cart/getTotals'][Object.keys(rootStore.getters['cart/getTotals']).length - 1].value);
    var panCountry = rootStore.state.config.aureatelabs.payment.panCountry;
    // Customer Information
    var cardholderName = rootStore.getters['checkout/getPersonalDetails'].firstName + ' ' + rootStore.getters['checkout/getPersonalDetails'].lastName;
    var version = rootStore.state.config.aureatelabs.payment.version;
    var xml = '';
    var paymentPayload = '';
    if (PaymentData.paymentToken !== undefined) {
      xml = '<PaymentRequest>'
      xml += '<merchantID>' + merchantID + '</merchantID>';
      xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
      xml += '<desc>' + desc + '</desc>';
      xml += '<amt>' + amt + '</amt>';
      xml += '<currencyCode>' + currencyCode + '</currencyCode>';
      xml += '<storeCardUniqueID>' + PaymentData.paymentToken + '</storeCardUniqueID>';
      xml += '<panCountry>' + panCountry + '</panCountry>';
      xml += '<cardholderName>' + PaymentData.cardholdername + '</cardholderName>';
      xml += '<encCardData></encCardData>';
      xml += '<storeCard>' + storeCard + '</storeCard>';
      xml += '</PaymentRequest>';
      paymentPayload = Base64.encode(xml);
    } else {
      xml = '<PaymentRequest>'
      xml += '<merchantID>' + merchantID + '</merchantID>';
      xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
      xml += '<desc>' + desc + '</desc>';
      xml += '<amt>' + amt + '</amt>';
      xml += '<currencyCode>' + currencyCode + '</currencyCode>';
      xml += '<panCountry>' + panCountry + '</panCountry>';
      xml += '<cardholderName>' + PaymentData.cardholdername + '</cardholderName>';
      xml += '<encCardData>' + PaymentData.encryptedCardInfo + '</encCardData>';
      xml += '</PaymentRequest>';
      paymentPayload = Base64.encode(xml);
    }

    var signature = crypto.createHmac('sha256', secretKey).update(paymentPayload).digest('hex').toUpperCase();
    var payloadXML = '<PaymentRequest>';
    payloadXML += '<version>' + version + '</version>';
    payloadXML += '<payload>' + paymentPayload + '</payload>';
    payloadXML += '<signature>' + signature + '</signature>';
    payloadXML += '</PaymentRequest>';
    var payload = Base64.encode(payloadXML);
    dispatch('submitPayment', payload)
  },
  processQRPayment ({ dispatch }, PaymentData: Payment) {
    var merchantID = rootStore.state.config.aureatelabs.payment.merchantID;
    var secretKey = rootStore.state.config.aureatelabs.payment.secretKey;

    // Transaction Information
    var desc = rootStore.state.config.aureatelabs.payment.desc;
    var uniqueTransactionCode = PaymentData.orderRef;
    var currencyCode = rootStore.state.config.aureatelabs.payment.currencyCode;
    let price = getGrandTotals()
    var amt = formatPrice(price);
    // var amt = formatPrice(rootStore.getters['cart/getTotals'][Object.keys(rootStore.getters['cart/getTotals']).length - 1].value);
    var panCountry = rootStore.state.config.aureatelabs.payment.panCountry; // Not required for APM payment

    // Customer Information
    var cardholderName = rootStore.getters['checkout/getPersonalDetails'].firstName + ' ' + rootStore.getters['checkout/getPersonalDetails'].lastName; // Not required for APM payment

    /** NOT REQUIRED FOR APM PAYMENT
    //Encrypted card data
    $encCardData = $_POST['encryptedCardInfo'];

    //Retrieve card information for merchant use if needed
    $maskedCardNo = $_POST['maskedCardInfo'];
    $expMonth = $_POST['expMonthCardInfo'];
    $expYear = $_POST['expYearCardInfo'];
    **/

    // Payment Options
    var paymentChannel = rootStore.state.config.aureatelabs.payment.paymentChannel; // Set transaction as Alternative Payment Method
    var agentCode = rootStore.state.config.aureatelabs.payment.agentCode; // APM agent code
    var channelCode = rootStore.state.config.aureatelabs.payment.channelCode; // APM channel code
    let date = new Date();
    date.setHours(date.getHours() + 3);
    // var paymentExpiry = (new Date()).toISOString().split('T')[0] + ' 23:59:59'; // pay slip expiry date (optional). format yyyy-MM-dd HH:mm:ss
    var paymentExpiry = date.toISOString().split('T')[0] + ' ' + date.toLocaleTimeString(); // pay slip expiry date (optional). format yyyy-MM-dd HH:mm:ss
    var mobileNo = rootStore.state.checkout.shippingDetails.phoneNumber;// customer mobile number
    var cardholderEmail = rootStore.getters['checkout/getPersonalDetails'].emailAddress; // customer email address

    // Request Information
    var version = '9.9';

    // Construct payment request message
    var xml = '<PaymentRequest>';
    xml += '<merchantID>' + merchantID + '</merchantID>';
    xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
    xml += '<desc>' + desc + '</desc>';
    xml += '<amt>' + amt + '</amt>';
    xml += '<currencyCode>' + currencyCode + '</currencyCode>';
    xml += '<panCountry>' + panCountry + '</panCountry>';
    xml += '<cardholderName>' + cardholderName + '</cardholderName>';
    xml += '<paymentChannel>' + paymentChannel + '</paymentChannel>';
    xml += '<agentCode>' + agentCode + '</agentCode>';
    xml += '<channelCode>' + channelCode + '</channelCode>';
    // xml += '<paymentExpiry>' + paymentExpiry + '</paymentExpiry>';
    xml += '<mobileNo>' + mobileNo + '</mobileNo>';
    xml += '<cardholderEmail>' + cardholderEmail + '</cardholderEmail>';
    xml += '<encCardData></encCardData>';
    xml += '</PaymentRequest>';
    var paymentPayload = Base64.encode(xml); // Convert payload to base64

    var signature = crypto.createHmac('sha256', secretKey).update(paymentPayload).digest('hex').toUpperCase();
    var payloadXML = '<PaymentRequest>';
    payloadXML += '<version>' + version + '</version>';
    payloadXML += '<payload>' + paymentPayload + '</payload>';
    payloadXML += '<signature>' + signature + '</signature>';
    payloadXML += '</PaymentRequest>';
    var payload = Base64.encode(payloadXML);
    dispatch('submitPayment', payload)
  },
  processPaymentRedirect ({ dispatch }, PaymentData: Payment) {
    const version = rootStore.state.config.aureatelabs.payment.redirect_version;
    const merchantID = rootStore.state.config.aureatelabs.payment.merchantID;
    const secretKey = rootStore.state.config.aureatelabs.payment.secretKey;
    const desc = rootStore.state.config.aureatelabs.payment.desc;
    const action = rootStore.state.config.aureatelabs.payment.creditcard_action
    const currencyCode = rootStore.state.config.aureatelabs.payment.currencyCode;
    const after_success_payment = rootStore.state.config.aureatelabs.payment.after_success_payment;
    const uniqueTransactionCode = PaymentData.orderRef;
    let price = getGrandTotals()
    var amt = formatPrice(price);
    const payment_option = 'CC';

    const allParam = version + merchantID + desc + uniqueTransactionCode + currencyCode + amt + after_success_payment + after_success_payment + payment_option
    const hash_value = crypto.createHmac('sha256', secretKey).update(allParam).digest('hex').toUpperCase();

    let payloadData = {
      action: action,
      version: version,
      merchantID: merchantID,
      secretKey: secretKey,
      paymentDescription: desc,
      amount: amt,
      currency: currencyCode,
      unique_id: uniqueTransactionCode,
      success_redirect: after_success_payment,
      hash_value: hash_value,
      payment_option: payment_option
    }
    dispatch('submitPaymentRedirect', payloadData)
  },
  submitPayment (context, payload) {
    var form = document.createElement('form');
    var element1 = document.createElement('input');

    form.method = 'POST';
    form.action = rootStore.state.config.aureatelabs.payment.action;

    element1.value = payload;
    element1.type = 'hidden';
    element1.name = 'paymentRequest';
    form.appendChild(element1);

    document.body.appendChild(form);

    form.submit();
  },
  submitPaymentRedirect (context, payload) {
    let form = document.createElement('form');
    form.method = 'POST';
    form.action = payload.action;

    let element1 = document.createElement('input');
    element1.value = payload.version;
    element1.type = 'hidden';
    element1.name = 'version';
    form.appendChild(element1);

    let element2 = document.createElement('input');
    element2.value = payload.merchantID;
    element2.type = 'hidden';
    element2.name = 'merchant_id';
    form.appendChild(element2);

    let element3 = document.createElement('input');
    element3.value = payload.currency;
    element3.type = 'hidden';
    element3.name = 'currency';
    form.appendChild(element3);

    let element4 = document.createElement('input');
    element4.value = payload.success_redirect;
    element4.type = 'hidden';
    element4.name = 'result_url_1';
    form.appendChild(element4);

    let element4_2 = document.createElement('input');
    element4_2.value = payload.success_redirect;
    element4_2.type = 'hidden';
    element4_2.name = 'result_url_2';
    form.appendChild(element4_2);

    let element5 = document.createElement('input');
    element5.value = payload.hash_value;
    element5.type = 'hidden';
    element5.name = 'hash_value';
    form.appendChild(element5);

    let element6 = document.createElement('input');
    element6.value = payload.paymentDescription;
    element6.type = 'hidden';
    element6.name = 'payment_description';
    form.appendChild(element6);

    let element7 = document.createElement('input');
    element7.value = payload.unique_id;
    element7.type = 'hidden';
    element7.name = 'order_id';
    form.appendChild(element7);

    let element8 = document.createElement('input');
    element8.value = payload.amount;
    element8.type = 'hidden';
    element8.name = 'amount';
    form.appendChild(element8);

    let element9 = document.createElement('input');
    element9.value = payload.payment_option;
    element9.type = 'hidden';
    element9.name = 'payment_option';
    form.appendChild(element9);

    document.body.appendChild(form);
    form.submit();
  },
  processPPPayment ({ dispatch }, PaymentData: Payment) {
    let promptpay = {
      'merchantID': '764764000001411',
      'secretKey': '9BA6B14D1ABE6D13DE1EF66E409AB6BAE948F1E30940726D4BC4EDAFBB98FB57',
      'desc': 'Phoenix Order Payment',
      'currencyCode': '764',
      'redirect_version': '8.5',
      'payment_option': 'PPQR',
      'creditcard_action': 'https://t.2c2p.com/RedirectV3/payment',
      'after_success_payment': 'https://admin.phoenixnext.com/p123payment/payment/ipn'
    };
    const { redirect_version: version, merchantID, secretKey, desc, creditcard_action: action, currencyCode, after_success_payment, payment_option } = promptpay
    const uniqueTransactionCode = PaymentData.orderRef;
    let price = getGrandTotals()
    var amt = formatPrice(price);
    const allParam = version + merchantID + desc + uniqueTransactionCode + currencyCode + amt + after_success_payment + after_success_payment + payment_option
    const hash_value = crypto.createHmac('sha256', secretKey).update(allParam).digest('hex').toUpperCase();
    let payloadData = {
      action: action,
      version: version,
      merchantID: merchantID,
      secretKey: secretKey,
      paymentDescription: desc,
      amount: amt,
      currency: currencyCode,
      unique_id: uniqueTransactionCode,
      success_redirect: after_success_payment,
      hash_value: hash_value,
      payment_option: payment_option
    }
    dispatch('submitPaymentRedirect', payloadData)
  }
}
export default actions
