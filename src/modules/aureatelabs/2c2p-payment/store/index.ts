import { Module } from 'vuex'
import PaymentState from '../types/PaymentState'
import actions from './actions'

export const paymentMethod: Module<PaymentState, any> = {
  namespaced: true,
  state: {
    items: { p2c2pPaymentDetails: '' }
  },
  mutations: {
    setPaymentDetails (state, payload) {
      state.items.p2c2pPaymentDetails = payload
    }
  },
  getters: {
    getPaymentDetails (state) {
      return state.items.p2c2pPaymentDetails
    }
  },
  actions
}
