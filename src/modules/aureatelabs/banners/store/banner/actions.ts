import { ActionTree } from 'vuex';
import BannerState from '../../types/BannerState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import SearchQuery from '@vue-storefront/core/lib/search/searchQuery';
import config from 'config';

const alBannerEntityName = config.aureatelabs.es_key;
const actions: ActionTree<BannerState, any> = {
  /**
   * Retrieve banners
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (
    context,
    {
      filterValues = null,
      filterField = 'id',
      size = 150,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { like: filterValues }
      });
    }
    if (
      skipCache ||
      !context.state.banners ||
      context.state.banners.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: alBannerEntityName,
        excludeFields,
        includeFields,
        sort: 'sort_order:asc'
      })
        .then(resp => {
          context.commit(types.BANNER_FETCH_BANNER, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'banner')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.banners;
        resolve(resp);
      });
    }
  }
};
export default actions;
