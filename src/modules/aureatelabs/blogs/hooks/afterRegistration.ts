import { Store } from 'vuex'
export function afterRegistration (app, store: Store<any>) {
    store.dispatch('blogs/list', {
      sort: 'creation_time:desc',
      skipCache: true
    })
    store.dispatch('blogs/getBlogsCategories', {
      filterValues: ''
    })
    if (app.$route.query?.page) {
      store.commit('blogs/blogs/BLOG_POST_CURRENT_PAGE', parseInt(app.$route.query.page))
    }
}
