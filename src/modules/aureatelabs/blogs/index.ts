import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { blogsModule } from './store/';
import { afterRegistration } from './hooks/afterRegistration'
// import { routes } from './router/index'
export const BlogsModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('blogs', blogsModule)
  // router.addRoutes(routes) // add rooutes for blog posts
  afterRegistration(app, store)
};
