import Blogs from '../pages/Blogs.vue'
import BlogDetail from '../pages/BlogDetail.vue'
import BlogsByTag from '../pages/BlogsByTag.vue'
import BlogsByCategory from '../pages/BlogsByCategory.vue'
import BlogSearch from '../pages/BlogSearch.vue'
export const routes = [
  { name: 'blog', path: '/guild', component: Blogs, meta: { layout: 'default', title: 'Guild' } },
  { name: 'blog-detail', path: '/guild/:slug', component: BlogDetail, meta: { layout: 'default' } },
  { name: 'blogs-by-tag', path: '/guild/tag/:tag', component: BlogsByTag, meta: { layout: 'default' } },
  { name: 'blogs-by-category', path: '/guild/category/:category', component: BlogsByCategory, meta: { layout: 'default' } },
  { name: 'blogs-search', path: '/guild-search', component: BlogSearch, meta: { layout: 'default' } }
]
