import { ActionTree } from 'vuex';
import * as types from './mutation-types'
import { Logger } from '@vue-storefront/core/lib/logger'
import { quickSearchByQuery } from '@vue-storefront/core/lib/search'
import SearchQuery from '@vue-storefront/core/lib/search/searchQuery'
import BlogsState from './BlogState'
import rootStore from '@vue-storefront/core/store'
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import i18n from '@vue-storefront/i18n'
import EventBus from '@vue-storefront/core/compatibility/plugins/event-bus'

const BLOG_ENTITY_NAME = 'magefan_blog_post'
const BLOG_CATEGORY_ENTITY_NAME = 'magefan_blog_category'
const BLOG_COMMENT_ENTITY_NAME = 'magefan_blog_post_comment'
const actions: ActionTree<BlogsState, any> = {
  list (context, { filterValues = null, filterField = 'identifier', size = 1500, start = 0, sort = '', excludeFields = null, includeFields = null, skipCache = false }) {
    let query = new SearchQuery()
   // query.applyFilter({ key: 'is_active', value: { 'eq': true } })
    if (filterValues) {
      query = query.applyFilter({ key: filterField, value: { 'like': filterValues } })
    }
    if (skipCache || (!context.state.posts || context.state.posts.length === 0)) {
      return quickSearchByQuery({ query, size: size, sort: sort, entityType: BLOG_ENTITY_NAME, excludeFields, includeFields })
        .then((resp) => {
          context.commit(types.BLOG_FETCH_POSTS, resp.items)
          context.commit(types.BLOG_FETCH_FEATURED_POSTS)
          return resp.items
        })
        .catch(err => {
          Logger.error(err, 'blogs')()
        })
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.posts
        resolve(resp)
      })
    }
  },

  getBlogsCategories (context, { filterValues = null, filterField = 'identifier', size = 1500, start = 0, sort = '', excludeFields = null, includeFields = null, skipCache = false }) {
    let query = new SearchQuery()
    query.applyFilter({ key: 'is_active', value: { 'eq': 1 } })
    if (filterValues) {
      query = query.applyFilter({ key: filterField, value: { 'like': filterValues } })
    }
    const hasSkipCache = context.getters.getBlogsCategoryList
    if (hasSkipCache.length <= 0) {
      skipCache = true
    }
    if (skipCache || (!context.state.categories || context.state.categories.length === 0)) {
      return quickSearchByQuery({ query, size: size, sort: sort, entityType: BLOG_CATEGORY_ENTITY_NAME, excludeFields, includeFields })
        .then((resp) => {
          context.commit(types.BLOG_FETCH_CATEGORIES, resp.items)
          return resp.items
        })
        .catch(err => {
          Logger.error(err, 'blogs-categories')()
        })
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.categories
        resolve(resp)
      })
    }
  },
  categorySelection (context, payload) {
    if (context.state.selectedCategory.includes(payload)) {
      context.commit(types.BLOG_REMOVE_CATS, payload) // removing category from selection
    } else {
      context.commit(types.BLOG_ACTIVE_CATS, payload) // adding category to selection
      if(context.getters.getBlogs.length < context.state.perPage) {
        context. commit(types.BLOG_CURRENT_PAGE, 1)
      }
    }
  },
  async searchKeyword ({ commit }, searchTxt) {
    let query = {
      'query': {
        'bool': {
          'must': [{
            'multi_match': {
              'query': searchTxt,
              'type': 'phrase_prefix',
              'fields': [
                'title',
                'content'
              ]
            }
          }]
        }
      }
    };
    return quickSearchByQuery({ query, size: 500, entityType: BLOG_ENTITY_NAME })
      .then((resp) => {
        return resp.items
      })
      .catch(err => {
        Logger.error(err, 'blogs')()
      })
  },
  async AddComment (context, commentData) {
    let url = rootStore.state.config.api.url + '/api/ext/aureatelabs/blog/comment'
    if (rootStore.state.config.storeViews.multistore) {
      url = adjustMultistoreApiUrl(url)
    }
    EventBus.$emit('notification-progress-start', i18n.t('Processing...'))
    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(commentData)
      })
      .then(res => res.json())
      .then(resp => {
        if(resp.code === 200) {
          rootStore.dispatch('notification/spawnNotification', {
            type: 'success',
            message: i18n.t('Comment Added!'),
            action1: { label: i18n.t('OK') }
          })
        } else {
          rootStore.dispatch('notification/spawnNotification', {
            type: 'error',
            message: resp.result.errorMessage,
            action1: { label: i18n.t('OK') }
          })
        }
      })
      EventBus.$emit('notification-progress-stop')
    } catch (e) {
      rootStore.dispatch('notification/spawnNotification', {
        type: 'error',
        message: i18n.t('Something went wrong. Try again in a few seconds.'),
        action1: { label: i18n.t('OK') }
      })
      EventBus.$emit('notification-progress-stop')
    };
  },
  getCommentsByPost({ commit }, post_id){
    let query = new SearchQuery()
    query.applyFilter({ key: 'status', value: { 'eq': 1 } })
    query.applyFilter({ key: 'post_id', value: { 'eq': post_id } })
    return quickSearchByQuery({ query, entityType: BLOG_COMMENT_ENTITY_NAME})
      .then((resp) => {
        return resp.items
      })
      .catch(err => {
        Logger.error(err, 'blogs-comments')()
      })
  }
}

export default actions
