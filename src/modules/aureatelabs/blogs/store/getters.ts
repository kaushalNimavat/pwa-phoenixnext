import { GetterTree } from 'vuex';
import BlogsState from './BlogState'
export const getters: GetterTree<BlogsState, any> = {
  getBlogsCategoryList: (state) => state.categories || [],
  getBlogList: (state) => state.posts || [],
  getBlogByIdentifier: (state) => (identifierKey) => state.posts.find(post => post.identifier === identifierKey),
  getBlogIds: (state) => {
    let blogIds = []
    state.posts.map(post => blogIds.push(post.id))
    return blogIds
  },
  getFeaturedBlogList: (state) => state.featuredPosts,
  getBlogByPostIds: (state) => (blogIds) => state.posts.filter((post) => {
    return blogIds.filter((id) => {
      return parseInt(id) === parseInt(post.post_id)
    }).length > 0
  }),
  activeCategory: (state) => state.selectedCategory || [],
  getBlogs: (state, getters) => {
    if (getters.activeCategory.length) { // check any selected category'blog available
      let idContainer = []
      let uniqueBlogsId = null
      getters.activeCategory.forEach(id => {
        const catPostIds = getters.getBlogsCategoryList.find(category => parseInt(category.category_id) === parseInt(id))
        if (catPostIds) {
          catPostIds.posts.map(id => idContainer.push(id))
        }
      })
      uniqueBlogsId = [...new Set(idContainer)] // remove duplicate blogs id
      let finalResult = getters.getBlogList.filter(post => uniqueBlogsId.includes(post.post_id))
      return finalResult.sort(function(a, b) {
        a = new Date(a.publish_time);
        b = new Date(b.publish_time);
        return a > b ? -1 : a < b ? 1 : 0;
      })
    } else {
      return getters.getBlogList.sort(function(a, b) {
        a = new Date(a.publish_time);
        b = new Date(b.publish_time);
        return a > b ? -1 : a < b ? 1 : 0;
      }) // return all category blogs
    }
  },
  getBlogsByTag: (state, getters) => (tag) => {
    return getters.getBlogList.filter(blog => blog.tags.includes(tag)) || []
  },
  getBlogByCurrentMonth: (state, getters) => {
    return getters.getFeaturedBlogList
  },
  getFirstBlogByPosition: (state, getters) => {
    let findFirstpositionBlog = getters.getBlogList.find(blog => blog.position === '1' && blog.include_in_recent === '0')
    return findFirstpositionBlog || getters.getFeaturedBlogList[0]
  },
  getBlogsByPosition: (state, getters) => {
    let positionedBlog = getters.getBlogList.filter(blog => blog.position !== '0' && blog.include_in_recent === '1')
    positionedBlog.sort((a, b) => parseInt(a.position) - parseInt(b.position))
    return positionedBlog || []
  }
  // getBlogsByDate: (state, getters) => {
  //   return getters.getBlogList.sort(function(a, b) {
  //     a = new Date(a.publish_time);
  //     b = new Date(b.publish_time);
  //     return a > b ? -1 : a < b ? 1 : 0;
  //   });
  // }
}
