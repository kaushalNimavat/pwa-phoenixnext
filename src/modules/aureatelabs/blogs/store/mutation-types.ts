export const SN_BLOG = 'blogs'
export const BLOG_FETCH_POSTS = SN_BLOG + '/FETCH_POSTS'
export const BLOG_FETCH_FEATURED_POSTS = SN_BLOG + '/FETCH_FEATURED_POSTS'
export const BLOG_FETCH_CATEGORIES = SN_BLOG + '/FETCH_CATEGORIES'
export const BLOG_ACTIVE_CATS = SN_BLOG + '/BLOG_ACTIVE_CATEGORIES'
export const BLOG_REMOVE_CATS = SN_BLOG + '/BLOG_REMOVE_CATEGORIES'
export const BLOG_CURRENT_PAGE = SN_BLOG + '/BLOG_POST_CURRENT_PAGE'
