import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.BLOG_FETCH_POSTS] (state, post) {
    state.posts = post || []
  },
  [types.BLOG_FETCH_FEATURED_POSTS] (state) {
    state.featuredPosts = state.posts.filter(post => parseInt(post.include_in_recent) === 1) || []
  },
  [types.BLOG_FETCH_CATEGORIES] (state, categories) {
    state.categories = categories || []
  },
  [types.BLOG_ACTIVE_CATS] (state, category) {
    state.selectedCategory.push(category)
  },
  [types.BLOG_REMOVE_CATS] (state, category) {
    const catIndex = state.selectedCategory.indexOf(category)
    state.selectedCategory.splice(catIndex, 1)
  },
  [types.BLOG_CURRENT_PAGE] (state, page) {
    state.currentPage = page
  }
}
