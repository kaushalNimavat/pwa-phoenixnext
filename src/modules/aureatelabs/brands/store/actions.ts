import { ActionTree } from 'vuex';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import SearchQuery from '@vue-storefront/core/lib/search/searchQuery';
import config from 'config';

const entityName = config.aureatelabs.brands_key;
const actions: ActionTree<any, any> = {
  /**
   * Retrieve banners
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (
    context, {
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    query.applyFilter({ key: 'is_active', value: { 'eq': '1' } });
    // query.applyFilter({ key: 'is_feature', value: { 'eq': '1' } });
    if (
      skipCache ||
      !context.state.brands ||
      context.state.brands.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: entityName,
        sort: 'sort_order:asc',
        size: 1000
      })
        .then(resp => {
          context.commit(types.AL_FETCH_BRANDS, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'brands')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.brands;
        resolve(resp);
      });
    }
  },
  getBrandBySlug (
    context,
    {
      filterValues = null,
      filterField = 'link',
      excludeFields = null,
      includeFields = null
    }
  ) {
    let brands = context.getters.getBrandsList;
    let currentBrands = brands.find(brand => brand.link === filterValues) || [];
    if (currentBrands.length > 0) {
      context.commit(types.AL_FETCH_BRANDS_BY_SLUG, currentBrands);
    } else {
      let query = new SearchQuery();
      if (filterValues) {
        query = query.applyFilter({
          key: filterField,
          value: { like: filterValues }
        });
      }
      return quickSearchByQuery({
        query,
        entityType: entityName,
        excludeFields,
        includeFields,
        sort: 'id:asc'
      })
        .then(resp => {
          context.commit(types.AL_FETCH_BRANDS_BY_SLUG, resp.items[0] || []);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'brands')();
        });
    }
  }

};
export default actions;
