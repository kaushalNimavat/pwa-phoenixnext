
import { GetterTree } from 'vuex';

export const getters: GetterTree<any, any> = {
  getBrandsList: (state) => state.brands,
  getBrandBySlug: (state) => state.brandBySlug,
  getIsFeaturedBrands: (state, getters) => {
    return getters.getBrandsList.filter(brand => parseInt(brand.is_feature) === 1) || []
  },
  getFeaturedBrandsBySorting: (state, getters) => {
    const brands = getters.getIsFeaturedBrands
    let getZeroBrands = brands.filter(brand => parseInt(brand.sort_order) === 0)
    let getWithoutZero = brands.filter(brand => parseInt(brand.sort_order) !== 0)
    getWithoutZero.sort((a, b) => a.sort_order - b.sort_order)
    return getWithoutZero.concat(getZeroBrands)
  }
}
