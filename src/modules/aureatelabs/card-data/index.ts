import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { cardDataModuleStore } from './store/index';

export const cardDataModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('cardData', cardDataModuleStore)
};
