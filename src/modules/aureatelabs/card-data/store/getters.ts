import CardDataState from '../types/CardDataState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<CardDataState, any> = {
  getCustomerSavedCard: (state) => state.cardData,
  getSaveCard: (state) => state.saveCard,
  getCustomerLevel: (state) => state.customerLevel
}
