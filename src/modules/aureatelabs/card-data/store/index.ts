import { Module } from 'vuex'
import CardDataState from '../types/CardDataState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const cardDataModuleStore: Module<CardDataState, any> = {
  namespaced: true,
  state: {
    cardData: [],
    saveCard: [],
    customerLevel: {}
  },
  mutations,
  actions,
  getters
}

