import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.CUSTOMER_SAVED_CARD] (state, cardData) {
    state.cardData = cardData || []
  },
  [types.CUSTOMER_SAVE_CARD] (state, saveCard) {
    state.saveCard = saveCard || []
  },
  [types.CUSTOMER_LEVEL] (state, payload) {
    state.customerLevel = payload || {}
  }
}
