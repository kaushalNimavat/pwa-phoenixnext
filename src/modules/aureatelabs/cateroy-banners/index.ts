import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { CategoryBannerModuleStore } from './store/banner/index';

export const CategoryBannerModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('categorybanner', CategoryBannerModuleStore)
};
