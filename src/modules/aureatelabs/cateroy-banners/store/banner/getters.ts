import CategoryBannerState from '../../types/CategoryBannerState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<CategoryBannerState, any> = {
  getCategoryBannerList: (state) => state.category_banners
}
