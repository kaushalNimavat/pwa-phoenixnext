import { Module } from 'vuex'
import CategoryBannerState from '../../types/CategoryBannerState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const CategoryBannerModuleStore: Module<CategoryBannerState, any> = {
  namespaced: true,
  state: {
    category_banners: []
  },
  mutations,
  actions,
  getters
}
