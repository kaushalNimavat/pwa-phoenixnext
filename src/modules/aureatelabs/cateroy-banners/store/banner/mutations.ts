import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.CATEGORY_BANNER_FETCH_BANNER] (state, banners) {
    state.category_banners = banners || []
  }
}
