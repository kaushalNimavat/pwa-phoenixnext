import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { customWishlistModuleStore } from './store/index';

export const CustomWishlistModule: StorefrontModule = function ({
  store
}) {
  store.registerModule('customWishlist', customWishlistModuleStore)
};
