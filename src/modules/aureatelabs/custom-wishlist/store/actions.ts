import Vue from 'vue'
import { ActionTree } from 'vuex';
import CustomWishlistState from '../types/CustomWishlistState';
import rootStore from '@vue-storefront/core/store'
import * as types from './mutation-types'
import { StorageManager } from '@vue-storefront/core/lib/storage-manager'
import { prepareQuery } from '@vue-storefront/core/modules/catalog/queries/common';

const actions: ActionTree<CustomWishlistState, any> = {
  async list (context, customerId) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.custom_wishlist.fetch;
    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ customer_id: customerId })
      }).then(res => res.json()).then((resp) => {
        let skus = []
        resp.result.items.forEach(product => {
          skus.push(product.product.sku)
        });
        context.dispatch('getProductList', skus)
        context.commit(types.WISHLIST_DATA_LIST, resp)
      });
    } catch (e) {
      console.log(e.message)
    }
  },
  async getProductList ({ state, rootState, dispatch, commit }, productIdsList) {
    try {
      let newProductsQuery = prepareQuery({ filters: [{ key: 'sku', value: { 'in': productIdsList } }] });
      await dispatch('product/list', { query: newProductsQuery, skipCache: true, size: 500 },
        {
          root: true
        }).then(res => {
        commit(types.CUSTOM_WISHLIST_DATA, res.items);
      }).catch(error => {
        console.error('There was an error!', error);
      });
    } catch (error) {
      console.log(error)
    }
  },
  async addProduct (context, parameters) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.custom_wishlist.add;

    let sku = parameters[0]
    let customerId = parameters[1]

    try {
      return fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ sku: sku, customer_id: customerId })
      }).then(resp => resp.json())
      .then(resp => { return resp.result })
    } catch (e) {
      console.log(e.message)
    }
  },
  async deleteProduct (context, parameters) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.custom_wishlist.remove;

    let itemId = parameters[0]
    let customerId = parameters[1]

    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ item_id: itemId, customer_id: customerId })
      })
    } catch (e) {
      console.log(e.message)
    }
  },
  async clearWishlist (context, customerId) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.custom_wishlist.clear;

    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ customer_id: customerId })
      })
    } catch (e) {
      console.log(e.message)
    }
  }
}

export default actions
