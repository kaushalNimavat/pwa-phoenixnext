import { Module } from 'vuex'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const customWishlistModuleStore: Module<any, any> = {
  namespaced: true,
  state: {
    customwishlist: [],
    wishlistdatalist: []
  },
  mutations,
  actions,
  getters
}
