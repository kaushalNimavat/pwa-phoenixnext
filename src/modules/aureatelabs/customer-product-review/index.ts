import { Logger } from '@vue-storefront/core/lib/logger'
import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';
import rootStore from '@vue-storefront/core/store'
import i18n from '@vue-storefront/i18n'

const customerProductReview = {
  namespaced: true,
  state: {
    customerReview: [],
    brandReviews: []
  },
  mutations: {
    GET_BRAND_REVIEW (state, payload) {
      state.brandReviews = payload
    }
  },
  actions: {
    async getCustomerReviews ({ commit }, payload) {
      let url = processLocalizedURLAddress('/api/ext/aureatelabs/customerReview/' + `${payload.customer_id}`);
      try {
        return await fetch(url, {
          method: 'GET',
          headers: {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          }
        })
          .then(res => res.json())
          .then((resp) => {
            Logger.info('Customer Review Response: ', resp)
            return resp
          });
      } catch (err) {
        Logger.error('Customer Review: ', err);
      }
    },
    async getCustomerBrandReviews ({ commit }, payload) {
      let url = processLocalizedURLAddress('/api/ext/aureatelabs/review/list/' + `${payload.brand_id}`);
      try {
        return await fetch(url, {
          method: 'GET',
          headers: {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          }
        })
          .then(res => res.json())
          .then((resp) => {
            commit('GET_BRAND_REVIEW', resp.result.items)
            Logger.info('Customer Review Response: ', resp)
            return resp
          });
      } catch (err) {
        Logger.error('Customer Review: ', err);
      }
    },
    async addBrandReview (context, payload) {
      let url = processLocalizedURLAddress('/api/ext/aureatelabs/review/add/');
      try {
        return await fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(payload)
        }).then(res => res.json()).then(res => res)
      } catch (e) {
        rootStore.dispatch('notification/spawnNotification', {
          type: 'error',
          message: i18n.t('Something went wrong. Try again in a few seconds.'),
          action1: { label: i18n.t('OK') }
        })
      };
    }
  }
}

export const customerProductReviewModule: StorefrontModule = function ({ store }) {
  store.registerModule('customerReview', customerProductReview)
}
