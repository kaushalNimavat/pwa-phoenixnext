
export default {
  install (Vue, options = {}) {
    const initEdroneScript = () => {
      let edroneScript = document.createElement('script')
      const sTags = document.getElementsByTagName('script')[0];
      edroneScript.setAttribute('type', 'text/javascript')
      edroneScript.setAttribute('async', true)
      edroneScript.setAttribute('src', 'https://d3bo67muzbfgtl.cloudfront.net/edrone_2_0.js?app_id=' + options.apiId)
      sTags.parentNode.insertBefore(edroneScript, sTags);
      edroneScript.onload = () => {
        window._edrone = window._edrone || {};
        window._edrone.app_id = options.apiId;
        window._edrone.version = '1.0.0';
        window._edrone.platform_version = '1.0.0';
        window._edrone.platform = 'custom';
      }
    }
    initEdroneScript()

    const initMailChimpScript = () => {
      let mailChimpScript = document.createElement('script')
      const sTags = document.getElementsByTagName('script')[0];
      mailChimpScript.setAttribute('type', 'text/javascript')
      mailChimpScript.setAttribute('async', true)
      mailChimpScript.setAttribute('src', 'https://chimpstatic.com/mcjs-connected/js/users/27718b60580ee3ba33b4cb47f/cc7681cf7b2dccc0559cf0f73.js')
      sTags.parentNode.insertBefore(mailChimpScript, sTags);
    }
    initMailChimpScript()
  }
};
