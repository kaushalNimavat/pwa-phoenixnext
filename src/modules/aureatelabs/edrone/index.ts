import Vue from 'vue';
import { Module } from 'vuex'
import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import edronePlugin from './edronePlugin'
import { isServer } from '@vue-storefront/core/helpers';
import { currentStoreView, localizedRoute } from '@vue-storefront/core/lib/multistore'
import { formatProductLink } from '@vue-storefront/core/modules/url/helpers'
import { getThumbnailPath } from '@vue-storefront/core/helpers'
import { quickSearchByQuery } from '@vue-storefront/core/lib/search'
export const KEY = 'edrone'

const EdroneModuleStore: Module<any, any> = {
  namespaced: true,
  state: {
    isProductView: null
  },
  mutations: {
    IS_PRODUCT_VIEW_PAGE (state, payload) {
      state.isProductView = payload
    }
  }
}

declare var window: any

async function getCategory (prodIds) {
  const { items } = await quickSearchByQuery({
    query: {
      'query': {
        'terms': {
          'id': prodIds
        }
      }
    },
    size: 100,
    includeFields: ['category']
  })
  let ids = []
  let names = []
  if (items && items.length) {
    items.forEach(item => {
      let tempIds = []
      let tempNames = []
      if (item?.category) {
        item.category && item.category.map(i => {
          tempIds.push(i.category_id)
          tempNames.push(i.name)
        })
        ids.push(tempIds.join('~'))
        names.push(tempNames.join('~'))
      }
    })
  }
  return {
    ids, names
  }
}

export const EdroneModule: StorefrontModule = function ({ store, appConfig }) {
  if (isServer) return;
  store.registerModule(KEY, EdroneModuleStore)
  Vue.use(edronePlugin, { initTimeout: 1000, apiId: appConfig.edrone.apiId })
  store.subscribe(async ({ type, payload }, state) => {
    window._edrone = window._edrone || {}
    let e = window._edrone
    const { width, height } = appConfig.products.thumbnails

    if (e && state.user.current) {
      const gender = ['M', 'F', 'Other']
      e.first_name = state.user.current.firstname
      e.last_name = state.user.current.lastname
      e.email = state.user.current.email
      e.birth_date = state.user.current.dob
      e.gender = gender[parseInt(state.user.current.gender) - 1]
    }

    if (type === 'cart/cart/ADD') {
      e.product_ids = payload.product.id
      e.product_skus = payload.product.sku
      e.product_titles = payload.product.name
      e.action_type = 'add_to_cart'
      e.init()
    }

    // product view
    if (type === 'edrone/IS_PRODUCT_VIEW_PAGE' && payload) {
      e.action_type = 'product_view';
      e.product_skus = payload.sku
      e.product_ids = payload.id
      e.product_titles = payload.name
      e.product_images = getThumbnailPath(payload.image)
      e.product_urls = localizedRoute(formatProductLink(payload, currentStoreView().storeCode))
      e.init()
    }

    // order confirmation
    if (type === 'order/orders/LAST_ORDER_CONFIRMATION') {
      const cartTotals = store.getters['cart/getTotals']
      const grandTotals = cartTotals.find(p => p.code === 'grand_total')
      let prodName = []
      let prodId = []
      let prodSku = []
      let prodImgs = []
      let prodUrls = []
      let prodCounts = []
      const { products, addressInformation } = payload.order
      products.map(p => {
        prodName.push(p.name)
        prodId.push(p.id)
        prodSku.push(p.sku)
        prodImgs.push(getThumbnailPath(p.image, width, height))
        prodUrls.push(localizedRoute(formatProductLink(p, currentStoreView().storeCode)))
        prodCounts.push(p.qty)
      })
      let categories = await getCategory(prodId)
      e.action_type = 'order';
      e.platform = 'custom';
      e.product_skus = prodSku.join('|')
      e.product_ids = prodId.join('|')
      e.product_titles = prodName.join('|')
      e.product_images = prodImgs.join('|')
      e.product_urls = prodUrls.join('|')
      e.order_id = payload.confirmation.orderNumber
      e.country = addressInformation.billingAddress.country_id
      e.city = addressInformation.billingAddress.city
      e.product_counts = prodCounts.join('|')
      e.base_currency = 'THB'
      e.order_currency = 'THB'
      e.product_category_names = categories.names.join('|')
      e.product_category_ids = categories.ids.join('|')
      e.base_payment_value = grandTotals?.value || 0;
      e.order_payment_value = grandTotals?.value || 0;
      e.init()
    }
  })
}
