import FreeGiftState from '../types/FreeGiftState'
import { GetterTree } from 'vuex';
import rootStore from '@vue-storefront/core/store';
import differenceBy from 'lodash/differenceBy'

export const getters: GetterTree<FreeGiftState, any> = {
  getFreeGiftProducts: (state) => state.freegifts || {},
  removedDuplicateGiftProducts: (state, getters) => {
    const currentCart = rootStore.getters['cart/getCartItems'] || []
    const removedMatchedProduct = differenceBy(getters.getFreeGiftProducts?.gifts || [], currentCart, 'id')
    return removedMatchedProduct || []
  },
  isAnyFreeProductAddedToCart: (state, getters) => {
    const freeProductsList = getters.getFreeGiftProducts?.gifts || []
    return freeProductsList.some(product => product.added === true) || false
  }
}
