import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import RootState from '@vue-storefront/core/types/RootState'
import config from 'config'
import { Module } from 'vuex'
import { Logger } from '@vue-storefront/core/lib/logger'
import moment from 'moment';
import fetch from 'isomorphic-fetch'

export const KEY = 'iterable'

const iterableModule: Module<{}, RootState> = {
	namespaced: true,
	actions: {
		async viewProducts({ }, payload) {
			try {
				let productData = {
					"email": payload.email,
					"eventName": "viewProducts",
					"createdAt": moment().unix(),
					"dataFields": {
						"viewedProductItems": [
							{
								"id": payload.product.id,
								"name": payload.product.name,
								"price": payload.product.special_price_incl_tax ? payload.product.special_price_incl_tax : payload.product.price_incl_tax,
								"quantity": 1,
								"imageUrl": payload.product.iterable_image,
								"url": payload.product.iterable_url
							}
						]
					}
				}
				await fetch('https://api.iterable.com/api/events/track', {
					method: 'POST',
					headers: {
						'Api-Key': config.aureatelabs.iterable.apiKey,
						'Accept': 'application/json, text/plain, */*',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(productData)
				})
					.then(response => response.json())
					.then(data => {
						if (data.code === 200) {
							Logger.info(data, 'iterable')()
						} else {
							Logger.error('Something went wrong. Try again in a few seconds.', 'iterable viewProducts')()
						}
					})

			} catch (e) {
				Logger.error('Something went wrong. Try again in a few seconds.', 'iterable')()
			}

		},
		async updateUser({ }, payload) {
			try {
				let userData = {
					"email": payload.email,
					"dataFields": {
						"viewedProductItems": [
							{
								"id": payload.product.id,
								"name": payload.product.name,
								"price": payload.product.special_price_incl_tax ? payload.product.special_price_incl_tax : payload.product.price_incl_tax,
								"quantity": 1,
								"imageUrl": payload.product.iterable_image,
								"url": payload.product.iterable_url
							}
						]
					}
				}
				await fetch('https://api.iterable.com/api/users/update', {
					method: 'POST',
					headers: {
						'Api-Key': config.aureatelabs.iterable.apiKey,
						'Accept': 'application/json, text/plain, */*',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(userData)
				})
					.then(response => response.json())
					.then(data => {
						if (data.code === 200) {
							Logger.info(data, 'iterable')()
						} else {
							Logger.error('Something went wrong. Try again in a few seconds.', 'iterable')()
						}
					})
			} catch (e) {
				Logger.error('Something went wrong. Try again in a few seconds.', 'iterable')()
			}
		}
	}
}

export const IterableModule: StorefrontModule = function ({ store, router, appConfig }) {
	store.registerModule(KEY, iterableModule)
}
