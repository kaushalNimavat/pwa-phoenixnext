import Vue from 'vue';
import { Module } from 'vuex'
import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import klaviyoPlugin from './klaviyoPlugin'
import { isServer } from '@vue-storefront/core/helpers';
import { currentStoreView, localizedRoute } from '@vue-storefront/core/lib/multistore'
import { formatProductLink } from '@vue-storefront/core/modules/url/helpers'
import { getThumbnailPath } from '@vue-storefront/core/helpers'
export const KEY = 'klaviyo'

const KlaviyoModuleStore: Module<any, any> = {
  namespaced: true,
  state: {
    isProductView: null
  },
  mutations: {
    IS_TRACK_PRODUCT_VIEW_PAGE (state, payload) {
      state.isProductView = payload
    }
  }
}

declare var window: any

export const KlaviyoModule: StorefrontModule = function ({ store, appConfig }) {
  if (isServer) return;
  store.registerModule(KEY, KlaviyoModuleStore)
  Vue.use(klaviyoPlugin, { initTimeout: 1000, apiId: appConfig.klaviyo.apiId })
  store.subscribe(async ({ type, payload }, state) => {
    var learnq = window?._learnq || [];
    const { width, height } = appConfig.products.thumbnails
    
    // track product view
    if (type === 'klaviyo/IS_TRACK_PRODUCT_VIEW_PAGE' && payload) {
      var item = {
        "ProductName": payload.name,
        "ProductID": payload.id,
        "SKU": payload.sku,
        "ImageURL": getThumbnailPath(payload.image, width, height),
        "URL": localizedRoute(formatProductLink(payload, currentStoreView().storeCode)),
        "Price": payload.price,
      };
      learnq.push(["track", "Viewed Product", item]);
    }
  })
}
