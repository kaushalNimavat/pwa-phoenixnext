export default {
  install (Vue, options = {}) {
    const initKlaviyoScript = () => {
      let klaviyoScript = document.createElement('script')
      const sTags = document.getElementsByTagName('script')[0];
      klaviyoScript.setAttribute('type', 'text/javascript')
      klaviyoScript.setAttribute('async', true)
      klaviyoScript.setAttribute('src', 'https://static.klaviyo.com/onsite/js/klaviyo.js?company_id=' + options.apiId)
      sTags.parentNode.insertBefore(klaviyoScript, sTags);
    }
    initKlaviyoScript()
  }
};
