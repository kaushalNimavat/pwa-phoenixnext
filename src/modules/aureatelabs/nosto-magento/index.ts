import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { NostoMagentoStore } from './store';

export const NostoMagentoModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('nostoMagento', NostoMagentoStore)
};
