import AddressBookState from '../types/NostoMagentoState'
import { ActionTree } from 'vuex';
import { Logger } from '@vue-storefront/core/lib/logger'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';
import * as types from './mutation-types'

export const actions: ActionTree<AddressBookState, any> = {
  /* get light novel product from magento */
  async getLightNovel (context, payload) {
    const days = payload
    const url = processLocalizedURLAddress(`/api/ext/aureatelabs/getCallLightNovel/${days}`)
    Logger.info('[getNostoLightNovelProduct] : API calling')
    try {
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
      }).then(res => res.json()).then((resp) => {
        context.commit(types.LIGHT_NOVEL_PRODUCT, resp.result.data);
      });
    } catch (e) {
      console.log(e.message)
    }
  },
  async getManga (context, payload) {
    const days = payload
    const url = processLocalizedURLAddress(`/api/ext/aureatelabs/getCallManga/${days}`)
    Logger.info('[getNostoBestSellerMangeProduct] : API calling')
    try {
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
      }).then(res => res.json()).then((resp) => {
        context.commit(types.BEST_SELLER_MANGA_PRODUCT, resp.result.data);
      });
    } catch (e) {
      console.log(e.message)
    }
  },
}
