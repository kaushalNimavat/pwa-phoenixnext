import AddressBookState from '../types/NostoMagentoState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<AddressBookState, any> = {
  getNostoLightNovelList: (state) => {
    return state.nostoLightNovelProduct
  },
  getNostoBestSellerMangaList: (state) => {
    return state.nostoBestSellerMangaProduct
  }
}
