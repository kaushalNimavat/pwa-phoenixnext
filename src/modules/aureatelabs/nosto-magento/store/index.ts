import { Module } from 'vuex'
import { actions } from './actions'
import NostoMagentoState from '../types/NostoMagentoState'
import { mutations } from './mutations'
import { getters } from './getters'

export const NostoMagentoStore: Module<NostoMagentoState, any> = {
  namespaced: true,
  state: {
    nostoLightNovelProduct: [],
    nostoBestSellerMangaProduct: []
  },
  mutations,
  actions,
  getters
}
