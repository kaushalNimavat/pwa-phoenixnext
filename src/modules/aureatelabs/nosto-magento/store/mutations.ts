import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.LIGHT_NOVEL_PRODUCT] (state, payload) {
    state.nostoLightNovelProduct = payload || []
  },

  [types.BEST_SELLER_MANGA_PRODUCT] (state, payload) {
    state.nostoBestSellerMangaProduct = payload || []
  }
}
