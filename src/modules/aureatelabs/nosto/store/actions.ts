import Vue from 'vue'
import { ActionTree } from 'vuex';
import NostoState from '../types/NostoState'
import NostoRecommendationState from '../types/NostoRecommendationState'
import * as types from './mutation-types';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search'
import { Logger } from '@vue-storefront/core/lib/logger'

declare const window: any;
const actions: ActionTree<NostoState, any> = {
  list (context, recommendationIds: NostoRecommendationState) {
    let prepareArray = {};
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewFrontPage()
          .setPlacements(recommendationIds)
          .load()
          .then(response => {
            context.commit(types.FETCH_ALL_NOSTOLIST, response.recommendations);
            // for (const [key, value] of Object.entries(response.recommendations)) {
            //   if (value['result_type'] === 'REAL') {
            //     var skus = [];
            //     var ids = [];
            //     for (const [divId, divValue] of Object.entries(value['products'])) {
            //       let pid = value['products'][divId].product_id;
            //       if (isNaN(pid)) {
            //         skus.push(pid)
            //       } else {
            //         ids.push(pid);
            //       }
            //     }
            //     prepareArray[key] = { skus: skus, ids: ids }
            //   }
            // }
            // context.commit(types.FETCH_ALL_RECOMMEND_PRODUCTS, response.recommendations);
            // context.dispatch('getProductList', prepareArray)
            return response.recommendations
          }).catch(error => {
            Logger.error('There was an error!', error)
          });
      });
    }
  },
  async getProductList ({ state, rootState, dispatch, commit }, productIdsList) {
    try {
      let resultArray = {};
      for (const [divId, dataType] of Object.entries(productIdsList)) {
        let querys = {
          'query': {
            'bool': {
              'should': [
                {
                  'terms': {
                    'id': Object.values(dataType['ids'])
                  }
                },
                {
                  'terms': {
                    'sku': Object.values(dataType['skus'])
                  }
                }
              ]
            }
          }
        }

        quickSearchByQuery({ query: querys, size: 100, entityType: 'product' })
          .then((resp) => {
            resultArray[divId] = resp.items
            commit(types.FETCH_ALL_RECOMMENDATIONS, resultArray);
          })
          .catch(err => {
            Logger.error('There was an error!', err)
          })
      };
    } catch (error) {
      Logger.error('There was an error!', error)
    }
  },
  sendOrderData (context, nostoData) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .addOrder(nostoData)
          .setPlacements(['order-related'])
          .load()
          .then(data => {
            console.log('Data Recommendations: ' + data.recommendations);
          }).catch(error => {
            console.error('There was an error!', error);
          })
      })
    }
  },
  viewFrontPage () {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewFrontPage()
          .setPlacements([
            'frontpage-nosto-most-popular',
            'frontpage-nosto-trending-products',
            'frontpage-nosto-featured-products',
            'frontpage-nosto-recommended-for-you',
            'frontpage-nosto-just-arrived',
            'frontpage-nosto-best-picked-products',
            'frontpage-nosto-best-sellers',
            'frontpage-nosto-selection-products',
            'frontpage-nosto-history-related',
            'frontpage-nosto-history'
          ])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewProduct (context, id) {
    let nostoId = context.rootState.nosto.clickednostoid;
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewProduct(id)
          .setRef(id, nostoId)
          .setPlacements([
            'frontpage-nosto-history',
            'frontpage-nosto-history-related'
          ])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewCategory (context, category) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewCategory(category)
          .setPlacements(['category-related'])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewSearch (context, query) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewSearch(query)
          .setPlacements([
            'search-related',
            'frontpage-nosto-selection-products',
            'frontpage-nosto-trending-products'
          ])
          .load()
          .then(data => {
            console.log(data.recommendations);
          })
      })
    }
  },
  viewCart (state) {
    let items = [];
    for (let i in state.rootState.cart.cartItems) {
      let product = state.rootState.cart.cartItems[i];
      items.push({
        name: product.name,
        price_currency_code: 'THB',
        product_id: product.sku,
        quantity: product.qty,
        sku_id: product.sku,
        unit_price: product.price
      })
    }
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .setCart({
            items: items
          })
          .viewCart()
          .update()
      })
    }
  },
  reportAddToCart (context, sku) {
    if (typeof window !== 'undefined' && typeof window.nostojs !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .reportAddToCart(sku, 'element')
          .update()
      })
    }
  },
  setNostoId (context, id) {
    context.commit(types.SET_CLICKED_NOSTO_ID, id);
  }
}

export default actions
