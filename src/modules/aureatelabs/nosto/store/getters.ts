
import NostoState from '../types/NostoState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<NostoState, any> = {
  getRecommendationList: (state) => {
    return state.recommendations
  },
  getNostoList: (state) => {
    return state.nostolist || []
  },
  getProductIds: (state) => {
    return state.ids
  }
}
