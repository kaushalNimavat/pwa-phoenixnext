import { MutationTree } from 'vuex'
import * as types from './mutation-types'
import NostoState from '../types/NostoState'

export const mutations: MutationTree<NostoState> = {
  [types.FETCH_ALL_RECOMMENDATIONS] (state, recommendations) {
    state.recommendations = recommendations || []
  },
  [types.FETCH_ALL_NOSTOLIST] (state, nostolist) {
    state.nostolist = nostolist || []
  },
  [types.FETCH_ALL_RECOMMEND_PRODUCTS] (state, ids) {
    state.ids = ids || []
  },
  [types.SET_CLICKED_NOSTO_ID] (state, id) {
    state.clickednostoid = id || 'frontpage-nosto-best-sellers'
  }
}
