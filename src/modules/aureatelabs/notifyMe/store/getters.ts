import NotifyMeState from '../types/NotifyMeState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<NotifyMeState, any> = {
  checkAlreadyNotify: (state) => (customerID, productID) => state.notifyUserInfo.find(notifyData => notifyData.customer_id === customerID && notifyData.product_id === productID)
}
