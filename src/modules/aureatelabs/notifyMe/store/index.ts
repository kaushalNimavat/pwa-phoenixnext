import { Module } from 'vuex'
import NotifyMeState from '../types/NotifyMeState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'

export const NotifyMeModuleStore: Module<NotifyMeState, any> = {
  namespaced: true,
  state: {
    notifyUserInfo: []
  },
  mutations,
  actions,
  getters
}
