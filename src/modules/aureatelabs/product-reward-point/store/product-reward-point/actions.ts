import Vue from 'vue'
import { ActionTree } from 'vuex';
import ProductRewardPointsState from '../../types/ProductRewardPointsState';
import rootStore from '@vue-storefront/core/store'
import * as types from './mutation-types'
import config from 'config'
import { StorageManager } from '@vue-storefront/core/lib/storage-manager'

const actions: ActionTree<ProductRewardPointsState, any> = {
  async list (context, productId) {
    let url = config.api.url + config.aureatelabs.aureate_product_rewards;
    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ product_id: productId })
      }).then(res => res.json()).then((resp) => {
        context.commit(types.PRODUCT_REWARD_FETCH_REWARD, resp);
      });
    } catch (e) {
      console.log(e.message)
    }
  }
}

export default actions
