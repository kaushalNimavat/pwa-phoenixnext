import { Module } from 'vuex'
import ProductRewardPointsState from '../../types/ProductRewardPointsState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const ProductRewardPointsModuleStore: Module<ProductRewardPointsState, any> = {
  namespaced: true,
  state: {
    productRewardPoints: []
  },
  mutations,
  actions,
  getters
}
