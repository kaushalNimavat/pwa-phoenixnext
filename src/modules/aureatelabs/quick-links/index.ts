import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { QuickLinksModuleStore } from './store/index';

export const QuickLinksModule: StorefrontModule = function ({
  store
}) {
  store.registerModule('quicklinks', QuickLinksModuleStore)
};
