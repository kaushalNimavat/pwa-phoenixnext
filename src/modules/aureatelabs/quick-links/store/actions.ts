import { ActionTree } from 'vuex';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import SearchQuery from '@vue-storefront/core/lib/search/searchQuery';
import config from 'config';

const entityName = config.aureatelabs.quicklinks_key;
const actions: ActionTree<any, any> = {
  /**
   * Retrieve banners
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (
    context,
    {
      filterValues = null,
      filterField = 'id',
      size = 150,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { like: filterValues }
      });
    }
    if (
      skipCache ||
      !context.state.quicklinks ||
      context.state.quicklinks.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: entityName,
        excludeFields,
        includeFields,
        sort: 'created_at.keyword:asc'
      })
        .then(resp => {
          context.commit(types.AL_FETCH_QUICK_LINKS, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'quicklinks')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.quicklinks;
        resolve(resp);
      });
    }
  }
};
export default actions;
