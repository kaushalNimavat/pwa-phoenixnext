import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.AL_FETCH_QUICK_LINKS] (state, quicklinks) {
    state.quicklinks = quicklinks || []
  }
}
