import Vue from 'vue'
import { ActionTree } from 'vuex'
import * as types from './mutation-types'
import RootState from '@vue-storefront/core/types/RootState'
import AuthState from '../types/AuthState'
import rootStore from '@vue-storefront/core/store'
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import i18n from '@vue-storefront/i18n'

const actions: ActionTree<AuthState, RootState> = {
  setLoginUser ({ commit }, user) {
    commit(types.SET_LOGIN_USER, user)
    if (typeof user !== 'string') {
      user = JSON.stringify(user)
    }
    return localStorage.setItem('user', user)
  },
  async singup ({ commit }, data) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.social.signup
    if (rootStore.state.config.storeViews.multistore) {
      url = adjustMultistoreApiUrl(url)
    }
    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
        .then(response => response.json())
        .then(data => {
          Vue.prototype.$bus.$emit('notification-progress-stop')
          Vue.prototype.$bus.$emit('modal-hide', 'modal-signup')
          if (data.code === 200) {
            rootStore.state.user.token = data.result;
            rootStore.dispatch('user/me', { refresh: true, useCache: false })
            rootStore.dispatch('user/getOrdersHistory', { refresh: true, useCache: false })
            rootStore.dispatch('notification/spawnNotification', {
              type: 'success',
              message: i18n.t('You are logged in!')
            });
          } else {
            rootStore.dispatch('notification/spawnNotification', {
              type: 'error',
              message: i18n.t('Something went wrong. Try again in a few seconds.')
            });
          }
        })
    } catch (e) {
      rootStore.dispatch('notification/spawnNotification', {
        type: 'error',
        message: i18n.t('Something went wrong. Try again in a few seconds.')
      })
    };
  }
}

export default actions
