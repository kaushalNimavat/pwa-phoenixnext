import { VueStorefrontModule } from '@vue-storefront/core/lib/module'
import { CatalogModule } from '@vue-storefront/core/modules/catalog'
import { CatalogNextModule } from '@vue-storefront/core/modules/catalog-next'
import { CartModule } from '@vue-storefront/core/modules/cart'
import { CheckoutModule } from '@vue-storefront/core/modules/checkout'
import { WishlistModule } from '@vue-storefront/core/modules/wishlist'
import { NotificationModule } from '@vue-storefront/core/modules/notification'
import { UrlModule } from '@vue-storefront/core/modules/url'
import { BreadcrumbsModule } from '@vue-storefront/core/modules/breadcrumbs'
import { UserModule } from '@vue-storefront/core/modules/user'
import { CmsModule } from '@vue-storefront/core/modules/cms'
import { GoogleTagManagerModule } from './google-tag-manager';
import { BannerModule } from './aureatelabs/banners'
import { CategoryBannerModule } from './aureatelabs/cateroy-banners'
import { PaymentBackendMethodsModule } from './payment-backend-methods'
import { NewsletterModule } from '@vue-storefront/core/modules/newsletter'
import { SocialAuthModule } from './aureatelabs/social-auth'
import { ContactUsModule } from './aureatelabs/contact-us'
import { PaymentModule } from './aureatelabs/2c2p-payment'
import { NostoModule } from './aureatelabs/nosto'
import { QuickLinksModule } from './aureatelabs/quick-links'
import { BrandsModule } from './aureatelabs/brands'
import { registerModule } from '@vue-storefront/core/lib/modules'
import { RewardPointsModule } from './aureatelabs/reward-points'
import { AddressBookModule } from './address-book'
import { ProductRewardPointsModule } from './aureatelabs/product-reward-point'
import { FreeGiftModule } from './aureatelabs/free-gift'
import { cardDataModule } from './aureatelabs/card-data'
import { CustomWishlistModule } from './aureatelabs/custom-wishlist'
import { customerProductReviewModule } from './aureatelabs/customer-product-review'
// import { EdroneModule } from './aureatelabs/edrone'
import { NotifyMeModule } from './aureatelabs/notifyMe';
import { KlaviyoModule } from './aureatelabs/klaviyo'
import { BlogsModule } from './aureatelabs/blogs'
import { NostoMagentoModule } from './aureatelabs/nosto-magento'
import { IterableModule } from './aureatelabs/iterable'

export function registerClientModules () {
  registerModule(UrlModule)
  registerModule(CatalogModule)
  registerModule(CheckoutModule) // To Checkout
  registerModule(CartModule)
  registerModule(PaymentBackendMethodsModule)
  registerModule(WishlistModule) // Trigger on wishlist icon click
  registerModule(NotificationModule)
  registerModule(UserModule) // Trigger on user icon click
  registerModule(CatalogNextModule)
  registerModule(BreadcrumbsModule)
  registerModule(CmsModule)
  registerModule(NewsletterModule)
  registerModule(BannerModule)
  registerModule(CategoryBannerModule)
  registerModule(ContactUsModule)
  registerModule(PaymentModule)
  registerModule(NostoModule)
  registerModule(QuickLinksModule)
  registerModule(BrandsModule)
  registerModule(RewardPointsModule)
  registerModule(AddressBookModule)
  registerModule(ProductRewardPointsModule)
  registerModule(FreeGiftModule)
  registerModule(cardDataModule)
  registerModule(CustomWishlistModule)
  registerModule(customerProductReviewModule)
  setTimeout(() => {
    registerModule(GoogleTagManagerModule)
  }, 10000);
  registerModule(SocialAuthModule)
  // registerModule(EdroneModule)
  registerModule(NotifyMeModule)
  registerModule(KlaviyoModule)
  registerModule(BlogsModule)
  registerModule(NostoMagentoModule)
  registerModule(IterableModule)
}

// Deprecated API, will be removed in 2.0
export const registerModules: VueStorefrontModule[] = [
]
