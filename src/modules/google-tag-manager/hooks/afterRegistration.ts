import Vue from 'vue'
import VueGtm from 'vue-gtm'
import { Store } from 'vuex'
import { i18n } from 'config'
import { isServer } from '@vue-storefront/core/helpers'
export const isEnabled = (gtmId: string | null) => {
  return typeof gtmId === 'string' && gtmId.length > 0 && !isServer
}
declare const window: any;
export function afterRegistration (config, store: Store<any>, router) {
  if (isEnabled(config.googleTagManager.id)) {
    const GTM: typeof VueGtm = (Vue as any).gtm

    const currencyCode = i18n.currencyCode
    const getProduct = (item) => {
      try {
        let product = {}

        const attributeMap: string[]|Record<string, any>[] = config.googleTagManager.product_attributes
        attributeMap.forEach(attribute => {
          const isObject = typeof attribute === 'object'
          let attributeField = isObject ? Object.keys(attribute)[0] : attribute
          let attributeName = isObject ? Object.values(attribute)[0] : attribute
          if (item.hasOwnProperty(attributeField) || product.hasOwnProperty(attributeName)) {
            let value = item[attributeField] || product[attributeName]
            if (value) {
              product[attributeName] = value
            }
          }
        })

        return product
      } catch (e) {
        console.log('Exception', e)
      }
    }

    store.subscribe(async ({ type, payload }, state) => {
      const user = state?.user?.current || '';
      const userData  =  {
        user_id_phoenixnext: user?.id || '',
        email: user?.email || '',
        first_name: user?.firstname || '',
        last_name: user?.lastname || ''
      }
      // Adding a Product to a Shopping Cart
      if (type === 'cart/cart/ADD') {
        try {
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            event: 'addToCart',
            ecommerce: {
              currencyCode: currencyCode,
              add: {
                products: [getProduct(payload.product)]
              },
              user: userData,
            }
          });
        } catch (e) {
          console.log('Exception', e)
        }
      }

      // Removing a Product from a Shopping Cart
      if (type === 'cart/cart/DEL') {
        try {
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            event: 'removeFromCart',
            ecommerce: {
              remove: {
                products: [getProduct(payload.product)],
              },
              user: userData,
            }
          });
        } catch (e) {
          console.log('Exception', e)
        }
      }

      // Measuring Views of Product Details
      if (type === 'google-tag-manager/productDetail') {
        try {
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            event: 'productDetail',
            ecommerce: {
              detail: {
                actionField: {
                  list: router.currentRoute.name
                },
                products: [getProduct(payload)],
                user: userData,
              }
            }
          });
        } catch (e) {
          console.log('Exception', e)
        }
      }

      if (type === 'google-tag-manager/productClick') {
        try {
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }

          GTM.trackEvent({
            event: 'productClick',
            ecommerce: {
              click: {
                actionField: {
                  list: router.currentRoute.name
                },
                products: [getProduct(payload)],
                user: userData,
              }
            }
          });
        } catch (e) {
          console.log('Exception', e)
        }
      }

      if (type === 'google-tag-manager/productImpression') {
        try {
          const products = payload.map(product => getProduct(product))
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            event: 'productImpression',
            ecommerce: {
              currencyCode: currencyCode,
              impressions: products,
              user: userData,
            }
          })
        } catch (e) {
          console.log('Exception', e)
        }
      }
      // Measuring Purchases
      if (type === 'order/orders/LAST_ORDER_CONFIRMATION') {
        try {
          if (payload.confirmation && payload.order) {
            try {
              const orderId = payload.confirmation.orderNumber
              const products = payload.order.products.map(product => getProduct(product))
              store.dispatch(
                'user/getOrdersHistory',
                { refresh: true, useCache: false }
              ).then(() => {
                const orderHistory = state.user.orders_history
                const order = state.user.orders_history ? orderHistory.items.find((order) => order['entity_id'].toString() === orderId) : null
                if (!isServer) {
                  window.dataLayer.push({ ecommerce: null })
                }
                GTM.trackEvent({
                  event: 'purchase',
                  'ecommerce': {
                    'currencyCode': currencyCode,
                    'purchase': {
                      'actionField': {
                        'id': orderId,
                        'affiliation': order ? order.store_name : '',
                        'revenue': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_grand_total ? state.cart.platformTotals.base_grand_total : '',
                        'tax': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_tax_amount ? state.cart.platformTotals.base_tax_amount : '',
                        'shipping': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_shipping_amount ? state.cart.platformTotals.base_shipping_amount : '',
                        'coupon': ''
                      },
                      'products': products,
                      'user': userData,

                    }
                  }
                })
              })
            } catch (e) {
              console.log('Exception', e)
            }
          }
        } catch (e) {
          console.log('Exception', e)
        }
      }

      if (type === 'url/URL/SET_CURRENT_ROUTE' && payload.name.search('checkout') >= 0) {
        try {
          let step = payload.hash.slice(1)
          const checkout_products = store.getters['cart/getCartItems'].map(product => getProduct(product))
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            'event': 'checkout',
            'ecommerce': {
              'currencyCode': currencyCode,
              'checkout': {
                'actionField': {
                  'step': step === 'shipping' ? 2 : step === 'payment' ? 3 : step === 'orderReview' ? 4 : 1,
                  'option': step !== '' ? step : 'personalDetails'
                },
                'products': checkout_products,
                'user': userData,
              }
            },
            'eventCallback': function () {
              // document.location.href = 'checkout';
            }
          })
        } catch (e) {
          console.log('Exception', e)
        }
      }

      // add_to_cart_recommend
      if (type === 'google-tag-manager/addToCartRecommend') {
        try {
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            event: 'add_to_cart_recommend',
            ecommerce: {
              currencyCode: currencyCode,
              add: {
                products: [getProduct(payload)],
                user: userData,
              }
            }
          });
        } catch (e) {
          console.log('Exception', e)
        }
      }

      // product_click_recommend
      if (type === 'google-tag-manager/productClickRecommend') {
        try {
          if (!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }

          GTM.trackEvent({
            event: 'product_click_recommend',
            ecommerce: {
              click: {
                actionField: {
                  list: router.currentRoute.name
                },
                products: [getProduct(payload)],
                user: userData,
              }
            }
          });
        } catch (e) {
          console.log('Exception', e)
        }
      }
    })
  }
}

