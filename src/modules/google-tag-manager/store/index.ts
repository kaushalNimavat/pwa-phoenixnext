import { Module } from 'vuex'
import GoogleTagManagerState from '../types/GoogleTagManagerState'

export const googleTagManagerModule: Module<GoogleTagManagerState, any> = {
  namespaced: true,
  state: {
    key: null
  },
  mutations: {
    productImpression (state, payload) {
    },
    productDetail (state, payload) {
    },
    productClick (state, payload) {
    },
    purchase (state, payload) {
    },
    transaction (state, payload) {
    }
  }
}
