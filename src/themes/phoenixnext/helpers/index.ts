import config from 'config'
import { currentStoreView } from '@vue-storefront/core/lib/multistore'
import map from 'lodash-es/map'
import uniq from 'lodash-es/uniq'
import difference from 'lodash-es/difference'
import { notifications } from '@vue-storefront/core/modules/cart/helpers'
import rootStore from '@vue-storefront/core/store'
import EventBus from '@vue-storefront/core/compatibility/plugins/event-bus'
import { getThumbnailPath } from '@vue-storefront/core/helpers'
import i18n from '@vue-storefront/i18n'

export function getPathForStaticPage (path: string) {
  const { storeCode } = currentStoreView()
  const isStoreCodeEquals = storeCode === config.defaultStoreCode

  return isStoreCodeEquals ? `/i${path}` : path
}

export function allowToaddProduct (product, productsInCart, productType = 'normal') {
  product.pre_stock_status = product?.pre_stock_status || getReadyOnly()
  if (productType === 'normal') {
    EventBus.$emit('notification-progress-start', i18n.t('Adding product to cart')+'....');
  }
  if (productsInCart && productsInCart.length > 0) {
    let cartItemStockStatus = uniq(map(productsInCart, 'pre_stock_status'));
    const { pre_stock_status } = product
    const preTitle = getPreOrderAttrOptions(product)
    if(!preTitle) return // Temporary condition because new attribute does not set for every products in backend
    const optionVal = preTitle?.value || ''
    if(cartItemStockStatus.includes(parseInt(optionVal)) && pre_stock_status === parseInt(optionVal) && productType=== 'freegift') {
      return true
    }

    if (pre_stock_status !== '' && pre_stock_status !== null && !cartItemStockStatus.includes(parseInt(pre_stock_status))) {
      let allowProductTypes = getPreOrderAttrOptions(productsInCart[0])
      let message = 'Please add only ' + (allowProductTypes?.label) + ' product(s)';
      rootStore.dispatch('notification/spawnNotification', notifications.createNotification({ type: 'error', message: i18n.t(message) }), { root: true })
      return false;
    }
  }
  return true;
}

export function brandsImgPath (image, width = 179, height = 179) {
  return getThumbnailPath('/' + image, width, height, 'brands')
}

export function replaceUrlLink (productLink) {
  return (productLink && productLink != null && productLink.replace) ? productLink.replace(/https:\/\/phoenixnext.com\/|https:\/\/www.phoenixnext.com\//g, '/') : '';
}

export function getPreOrderAttrOptions (product) {
  const preStockStatus = product?.pre_stock_status || getReadyOnly()
  const allAttr = (rootStore.getters['attribute/getAttributeListByCode'])?.pre_stock_status || null
  if(allAttr) {
    return allAttr.options.find(item => parseInt(item.value) === parseInt(preStockStatus))
  }
  return null
}

export function getReadyOnly() {
  const allAttr = (rootStore.getters['attribute/getAttributeListByCode'])?.pre_stock_status || null
  const readyStatus =  allAttr.options.find(item => item.label === 'Ready')
  return parseInt(readyStatus.value);
}
