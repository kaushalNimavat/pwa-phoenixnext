const Home = () => import(/* webpackChunkName: "vsf-home" */ 'theme/pages/Home.vue')
const PageNotFound = () => import(/* webpackChunkName: "vsf-not-found" */ 'theme/pages/PageNotFound.vue')
const ErrorPage = () => import(/* webpackChunkName: "vsf-error" */ 'theme/pages/Error.vue')
const Product = () => import(/* webpackChunkName: "vsf-product" */ 'theme/pages/Product.vue')
const Category = () => import(/* webpackChunkName: "vsf-category" */ 'theme/pages/Category.vue')
const CmsPage = () => import(/* webpackChunkName: "vsf-cms" */ 'theme/pages/CmsPage.vue')
const ElementPage = () => import(/* webpackChunkName: "vsf-element" */ 'theme/pages/ElementPage.vue')
// const Checkout = () => import(/* webpackChunkName: "vsf-checkout" */ 'theme/pages/Checkout.vue')
const MyAccount = () => import(/* webpackChunkName: "vsf-my-account" */ 'theme/pages/MyAccount.vue')
const Contact = () => import(/* webpackChunkName: "vsf-contact" */ 'theme/pages/Contact.vue')
const BrandLandingPage = () => import(/* webpackChunkName: "vsf-BrandLandingPage" */ 'theme/pages/Brands.vue')
const BrandDetail = () => import(/* webpackChunkName: "vsf-BrandDetail" */ 'theme/pages/BrandDetail.vue')
const BrandProductList = () => import(/* webpackChunkName: "vsf-BrandProductList" */ 'theme/pages/BrandProductList.vue')
const ThankYou = () => import(/* webpackChunkName: "vsf-BrandDetail" */ 'theme/pages/ThankYou.vue')
const Faqs = () => import(/* webpackChunkName: "vsf-faqs" */ 'theme/pages/Faqs.vue')
const History = () => import(/* webpackChunkName: "vsf-BrandDetail" */ 'theme/pages/History.vue')
const NostoProducts = () => import(/* webpackChunkName: "vsf-BrandDetail" */ 'theme/pages/NostoProducts.vue')
const ResetPassword = () => import(/* webpackChunkName: "vsf-reset-password" */ 'theme/pages/ResetPassword.vue');
const Blogs = () => import(/* webpackChunkName: "vsf-blogs" */ 'src/modules/aureatelabs/blogs/pages/Blogs.vue');
const BlogDetail = () => import(/* webpackChunkName: "vsf-blogs" */ 'src/modules/aureatelabs/blogs/pages/BlogDetail.vue');
const BlogsByTag = () => import(/* webpackChunkName: "vsf-blogs" */ 'src/modules/aureatelabs/blogs/pages/BlogsByTag.vue');
const BlogsByCategory = () => import(/* webpackChunkName: "vsf-blogs" */ 'src/modules/aureatelabs/blogs/pages/BlogsByCategory.vue');
const BlogSearch = () => import(/* webpackChunkName: "vsf-blogs" */ 'src/modules/aureatelabs/blogs/pages/BlogSearch.vue');
const NewCheckout = () => import(/* webpackChunkName: "vsf-blogs" */ 'theme/pages/NewCheckout.vue');
const Login = () => import(/* webpackChunkName: "vsf-blogs" */ 'theme/pages/Login.vue');
const Register = () => import(/* webpackChunkName: "vsf-blogs" */ 'theme/pages/Register.vue');
let routes = [
  { name: 'home', path: '/', component: Home, alias: '/pwa.html' },
  { name: 'checkout', path: '/checkout', component: NewCheckout },
  { name: 'my-account', path: '/my-account', component: MyAccount },
  { name: 'my-level', path: '/my-account/level', component: MyAccount, props: { activeBlock: 'MyLevel' } },
  { name: 'my-shipping-details', path: '/my-account/shipping-details', component: MyAccount, props: { activeBlock: 'MyShippingDetails' } },
  { name: 'my-newsletter', path: '/my-account/newsletter', component: MyAccount, props: { activeBlock: 'MyNewsletter' } },
  { name: 'my-orders', path: '/my-account/orders', component: MyAccount, props: { activeBlock: 'MyOrders' } },
  { name: 'my-order', path: '/my-account/orders/:orderId', component: MyAccount, props: { activeBlock: 'MyOrder' } },
  { name: 'my-recently-viewed', path: '/my-account/recently-viewed', component: MyAccount, props: { activeBlock: 'MyRecentlyViewed' } },
  { name: 'my-reward-points', path: '/my-account/my-reward-points', component: MyAccount, props: { activeBlock: 'MyRewardPoints' } },
  { name: 'my-reviews', path: '/my-account/my-reviews', component: MyAccount, props: { activeBlock: 'MyReviews' } },
  { name: 'contact', path: '/contact', component: Contact, props: { page: 'contact', title: 'Contact' } },
  { name: 'error', path: '/error', component: ErrorPage },
  { name: 'virtual-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'bundle-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'simple-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'downloadable-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'grouped-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'configurable-product', path: '/p/:parentSku/:slug/:childSku', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'product', path: '/p/:parentSku/:slug/:childSku', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'category', path: '/c/:slug', component: Category },
  { name: 'cms-page', path: '/i/:slug', component: CmsPage },
  { name: 'element-page', path: '/element-page', component: ElementPage, props: { page: 'element-page', title: 'Element Page' } },
  { name: 'brands', path: '/series', component: BrandLandingPage },
  { name: 'brand-detail', path: '/series/:slug', component: BrandDetail },
  { name: 'brand-product-list', path: '/series/:slug/:catslug', component: BrandProductList },
  { name: 'thank-you', path: '/thank-you', component: ThankYou },
  { name: 'faqs', path: '/faqs', component: Faqs, props: { block: 'faq', title: 'FAQs' } },
  // { name: 'career', path: '/career', component: Faqs, props: { block: 'career', title: 'อาชีพ' } },
  { name: 'career', path: '/career', component: Faqs, props: { block: 'career', title: 'ตำแหน่งงานว่าง' } },
  { name: 'history', path: '/history', component: History },
  { name: 'nosto-products', path: '/products/:slug', component: NostoProducts },
  { name: 'create-password', path: '/create-password', component: ResetPassword },
  { name: 'page-not-found', path: '*', component: PageNotFound },
  { name: 'blogs', path: '/guild', component: Blogs, props: { title: 'Guild' } },
  { name: 'blog-detail', path: '/guild/:slug', component: BlogDetail },
  { name: 'blogs-by-tag', path: '/guild/tag/:tag', component: BlogsByTag },
  { name: 'blogs-by-category', path: '/guild/category/:category', component: BlogsByCategory },
  { name: 'blogs-search', path: '/guild-search', component: BlogSearch },
  { name: 'login', path: '/login', component: Login },
  { name: 'register', path: '/register', component: Register }
]

export default routes
